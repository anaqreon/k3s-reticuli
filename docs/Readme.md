
After k3s is installed and running, modify ``/etc/systemd/system/k3s.service`` to run k3s without deploying Traefik. (Alternatively, initially you can install k3s with the option that disables Traefik.)
```
...
ExecStart=/usr/local/bin/k3s \
  server \
  --disable traefik \
```

Then apply and reload the k3s service:
```
ubuntu@lsst-sandbox:~$ sudo systemctl daemon-reload
ubuntu@lsst-sandbox:~$ sudo service k3s restart
```
